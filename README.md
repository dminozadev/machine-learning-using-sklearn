# MIT Project Classifier Score
Project made of python, this is for project purposes and would love to play around also.

Sample project is in MIT Project Classifier Scores.ipynb

## Running the environment
I used pipenv for making my own environment on my local, to install it. Just follow these steps [here](https://medium.com/@mahmudahsan/how-to-use-python-pipenv-in-mac-and-windows-1c6dc87b403e)

If you dont want to use pipenv, you can use the traditional virtualenv. you can do the steps here for [windows](https://pymote.readthedocs.io/en/latest/install/windows_virtualenv.html) and for [mac](http://sourabhbajaj.com/mac-setup/Python/virtualenv.html) and last but not the least [linux](https://askubuntu.com/questions/244641/how-to-set-up-and-use-a-virtual-python-environment-in-ubuntu)

